<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Exchange Currency</title>
</head>
<body>
	<h1>Currency Exchange Rate</h1>
	<div>Select base</div>
	<select id="baseSelect">
		<option id="UAH" selected="selected">Українська гривня</option>
		<option id="AUD">Австралійський долар</option>
		<option id="AZN">Азербайджанський манат</option>
		<option id="GBP">Англійський фунт стерлінгів</option>
		<option id="BYR">Бiлоруський рубль</option>
		<option id="DKK">Данська крона</option>
		<option id="USD">Долар США</option>
		<option id="EUR">Євро</option>
		<option id="ISK">Ісландська крона</option>
		<option id="KZT">Казахстанський теньге</option>
		<option id="CAD">Канадський долар</option>
		<option id="MDL">Молдовський лей</option>
		<option id="NOK">Норвезька крона</option>
		<option id="PLN">Польський злотий</option>
		<option id="RUB">Російський рубль</option>
		<option id="SGD">Сінгапурський долар</option>
		<option id="XDR">Спецiальнi права запозичення</option>
		<option id="TRY">Турецька ліра</option>
		<option id="TMT">Туркменський манат</option>
		<option id="HUF">Угорський форинт</option>
		<option id="UZS">Узбецький сум</option>
		<option id="CZK">Чеська крона</option>
		<option id="SEK">Шведська крона</option>
		<option id="CHF">Швейцарський франк</option>
		<option id="CNY">Юань Женьмiньбi</option>
		<option id="JPY">Японська єна</option>
	</select>
	<div>
		Select date <input type="date" id="date" value="2016-05-29">
	</div>
	<div>Show mode</div>
	<div>
		<input type="radio" name="mode" checked onclick="hideCustomSelection()">All
	</div>
	<div>
		<input type="radio" name="mode" onclick="showCustomSelection()" id="customMode">Custom
	</div>
	<div style="margin-left : 20px; display : none;" id="custom">
		<div>Choose interested currency</div>
		<input type="checkbox" checked id="AUD">Австралійський долар</br>
		<input type="checkbox" checked id="AZN">Азербайджанський манат</br>
		<input type="checkbox" checked id="GBP">Англійський фунт стерлінгів</br>
		<input type="checkbox" checked id="BYR">Бiлоруський рубль</br>
		<input type="checkbox" checked id="DKK">Данська крона</br>
		<input type="checkbox" checked id="USD">Долар США</br>
		<input type="checkbox" checked id="EUR">Євро</br>
		<input type="checkbox" checked id="ISK">Ісландська крона</br>
		<input type="checkbox" checked id="KZT">Казахстанський теньге</br>
		<input type="checkbox" checked id="CAD">Канадський долар</br>
		<input type="checkbox" checked id="MDL">Молдовський лей</br>
		<input type="checkbox" checked id="NOK">Норвезька крона</br>
		<input type="checkbox" checked id="PLN">Польський злотий</br>
		<input type="checkbox" checked id="RUB">Російський рубль</br>
		<input type="checkbox" checked id="SGD">Сінгапурський долар</br>
		<input type="checkbox" checked id="XDR">Спецiальнi права запозичення</br>
		<input type="checkbox" checked id="TRY">Турецька ліра</br>
		<input type="checkbox" checked id="TMT">Туркменський манат</br>
		<input type="checkbox" checked id="HUF">Угорський форинт</br>
		<input type="checkbox" checked id="UZS">Узбецький сум</br>
		<input type="checkbox" checked id="CZK">Чеська крона</br>
		<input type="checkbox" checked id="SEK">Шведська крона</br>
		<input type="checkbox" checked id="CHF">Швейцарський франк</br>
		<input type="checkbox" checked id="CNY">Юань Женьмiньбi</br>
		<input type="checkbox" checked id="JPY">Японська єна</br>
	</div>
	<input type="button" value="Search" onclick="search()">
	</br>
	<h2>Result</h2>
	<div id="result"></div>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" async>
		window.onload = function(){
			$.ajax({
				type: 'GET',
		        url: '/ua',
		        complete: function (resp) {
		           console.log(resp);
		           showResult(resp.responseText.replaceAll(/\\/,''));
		        }
			});
		}
		String.prototype.replaceAll = function(search, replace) {
			return this.split(search).join(replace);
		}
		function showResult(strResponse){
			var jsonResponse = JSON.parse(strResponse);
			var div = document.createElement('div');
			div.innerHTML = 'Base : ' + jsonResponse.base + '</br>'
				+ 'Date : ' + jsonResponse.date;
			var table = document.createElement('table');
			for(var i = 0; i < jsonResponse.currenciesRate.length; i++){
				var row = document.createElement('tr');
				row.innerHTML = '<td>' + jsonResponse.currenciesRate[i].idString + '</td>' +
					'<td>' + jsonResponse.currenciesRate[i].amount + '</td>' +
					'<td>' + jsonResponse.currenciesRate[i].name + '</td>' +
					'<td>' + jsonResponse.currenciesRate[i].exchangeRate + '</td>';
				table.appendChild(row);
			}
			var result = document.getElementById('result');
			result.innerHTML = '';
			if(result){
				result.appendChild(div);
				result.appendChild(table);
			}
		}
		function showCustomSelection(){
			var section = document.getElementById('custom');
			section && (section.style.display = 'block');
		}
		function hideCustomSelection(){
			var section = document.getElementById('custom');
			section && (section.style.display = 'none');
		}
		function search(){
			var base = document.getElementById('baseSelect');
			base && (base=base.options[base.selectedIndex].id);
			var date = document.getElementById('date');
			if(date){
				date = date.value.split('-');
				date = date[2] + '.' + date[1] + '.' + date[0];
			} 
			var mode = document.getElementById('customMode');
			var url = '/custom/'+base+'/'+date;
			var type = 'GET';
			if(mode && mode.checked){
				var result = $("input:checked").map(function () {if(this.id != 'customMode')return this.id;}).get().join(",");
				url += '/'+result;
				type = 'POST';
			}
			$.ajax({
				type: type,
		        url: url,
		        complete: function (resp) {
		           console.log(resp);
		           showResult(resp.responseText.replaceAll(/\\/,''));
		        }
			});
		}
		
	</script>
</body>
</html>
