package com.epam.exchange.currency.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.epam.exchange.currency.model.Currency;
import com.epam.exchange.currency.model.CurrencyProducer;
import com.epam.exchange.currency.util.CurrencyExchanger;

@Component
@Path("/")
public class MainController {
	
	@Path("/ua")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CurrencyExchanger getAllByDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		CurrencyProducer currencyProducer = new CurrencyProducer(dateFormat.format(date));
		CurrencyExchanger exchanger = new CurrencyExchanger();
		exchanger.setBase("UAH");
		exchanger.setDate(currencyProducer.getDate());
		exchanger.setCurrenciesRate(currencyProducer.transformHtmlDataIntoCurrecyObj().values());
        return exchanger;
    }
	
	@Path("/custom/{base}/{date}/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CurrencyExchanger getAllByDateAndBase(@PathParam("base") String base, @PathParam("date") String date) {
		CurrencyProducer currencyProducer = new CurrencyProducer(date);
		CurrencyExchanger exchanger = new CurrencyExchanger();
		exchanger.setBase(base);
		exchanger.setDate(currencyProducer.getDate());
		exchanger.setCurrenciesRate(currencyProducer.changedBaseResult(base).values());
        return exchanger;
    }
	
	@Path("/custom/{base}/{date}/{currecies}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public CurrencyExchanger getCustomByDateAndBase(@PathParam("base") String base, @PathParam("date") String date, @PathParam("currecies") String currencies) {
		CurrencyProducer currencyProducer = new CurrencyProducer(date);
		CurrencyExchanger exchanger = new CurrencyExchanger();
		exchanger.setBase(base);
		exchanger.setDate(currencyProducer.getDate());
		List<String> interestedCurencies = Arrays.asList(currencies.split(","));
		Collection<Currency> allCurrencies = currencyProducer.changedBaseResult(base).values();
		Iterator<Currency> iterator = allCurrencies.iterator();
		while(iterator.hasNext()){
			Currency current = iterator.next();
			if(!interestedCurencies.contains(current.getIdString())){
				iterator.remove();
			}
		}
		exchanger.setCurrenciesRate(allCurrencies);
        return exchanger;
    }
}